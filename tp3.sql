create or replace function t3 (
    le_jour sejour.jour%type
) return int 
is
    le_nb int;
begin
    SELECT COUNT(*)
        INTO le_nb
        FROM sejour
        WHERE jour < le_jour;
    DELETE sejour
        WHERE jour < le_jour;
    return le_nb;
end;
/

create or replace procedure t3_proc(
	le_jour sejour.jour%type,
	le_nombre out int
)
is
begin
	select count(*)
		into le_nombre
		from sejour
		where jour<le_jour;
	delete sejour
		where jour<le_jour;
end;
/

-- exec dbms_output.put_line(t3(64));


declare
    nombre int;
begin
    t3_proc(3360,nombre);
    dbms_output.put_line(nombre);
end;
/

create or replace function traitement1(
    le_nom client.nom%type,
    l_age client.age%type
) return client.idc%type;
is
    l_idc client.idc%type;
begin
    l_idc := seq_client.nextval;
    INSERT INTO client(idc, nom, age)
        VALUES(l_idc, le_nom, l_age);
    return l_idc;
end;
/